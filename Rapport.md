# Rapport AN3D
## 20 Novembre 2020
Thibault BUATOIS
Baptiste PARSY

## Table des matières

- [Introduction](#Introduction)
- [Version Naïve](#Version_Naive)
    - [Collision sphère-plan](#NCSP)
    - [Collision sphère-sphère](#NCSS)
- [Amélioration de la collision sphère-plan](#ACSP)
- [Amélioration de la collision sphère-sphère](#ACSS)
    - [Collisions élastiques](#ACE)
    - [Contacts statiques]()
- [Implémentation d'un système de masse](#ISM)
- [Rotation du cube](#RC)

<a name="Introduction"></a>

## Introduction : Collisions, sphères et attraction

Pour notre sujet de projet, nous avons choisis de travailler sur les sphères et leurs collisions avec des plans (un cube) ainsi qu'entre elles. Ce rapport détaillera les différentes implémentations que nous avons faites, et les commentaires que nous pouvons faire dessus. Et si vous vous demandez à quoi correspond `attraction` dans le titre, nous vous demandons de patienter jusqu'à la dernière version de notre projet.

<a name="Version_Naive"></a>

## Version Naive, sans lire les slides

Notre première version, faite en TP, est la version la plus naïve possible.
<a name="NCSP"></a>

#### Collision sphère-plan

Le premier point à noter pour cette version concernant la collision entre sphère et plan est la façon de détecter la collision. Si l'on pose `ptc` une sphère avec `ptc.p` sa position et `ptc.r` son rayon, alors notre détection de collision était une comparaison coordonnée par coordonnée entre `ptc.p` et `-1 + ptc.r` d'un côté et `1 - ptc.r` de l'autre. Cela faisait suite à la position des plans, qui se retrouvaient donc codées en dur avec ces comparaisons.

La gestion de la collision est tout aussi "intéressante", puis que la vitesse est recalculée avec l'opposée de chaque composante si, dans le cas `-1 + ptc.r` (respectivement `1 - ptc.r`), la composante est négative (respectivement positive). Cette version naïve de la collision donne des résultats corrects, mais présente de nombreuses faiblesses, notamment sur le réalisme de la collision.

<a name="NCSS"></a>
#### Collision sphère-sphère

La détection de collision sphère-sphère de cette version naïve est la seule chose qui est restée au delà de ladite version. Une collision arrive lorsque la distance entre deux sphères est inférieure à la somme de leur rayon.

Pour ce qui est de la gestion de la collision, seules les positions sont modifiées et sont reprojetées selon la direction entre les deux sphères. Encore une fois, cette méthode fonctionne dans le sens où on peut observer des collisions mais la réaction des sphères n'est absolument pas réaliste.

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/0LUusPe.gifv#embed"></iframe>
(commit eabe9641)

<a name="ACSP"></a>

## Amélioration de la collision sphère-plan
### Ou comment nous avons eu un cours et lu les slides

La première étape pour une meilleure détection de la collision entre une sphère et un plan est d'avoir une représentation du plan. Nous avons crée un type `struct plane` : 
```c++
struct plane {
    vcl::vec3 n; // Normal
    vcl::vec3 a; // Point
}
```
Les six plans du cube sont ajoutés dans un `std::vector` lors de l'appel à la méthode `scene_model::setup_data`.
La seconde étape est la modification du critère de détection de collision. Nous avons implémenté le critère présenté en cours : 
$$
(ptc.p - plane.a) \cdot plane.n \gt ptc.r \rightarrow Pas~de~collision\\
(ptc.p - plane.a) \cdot plane.n \le ptc.r \rightarrow collision
$$
La gestion de la collision est aussi tirée des slides, en utilisant vitesse tangentielle et orthogonale, notées respectivement `v_par` et `v_ortho`.
Ainsi la nouvelle vitesse de la sphère est définie par :
$$
ptc.v = \alpha v_{par} - \beta v_{ortho}
$$
avec $\alpha$ et $\beta$ les coefficients de restitution lié à la friction et à l'impact, compris entre 0 et 1. Ces coefficients sont initialisés respectivement à $0.9$ et $0.7$, mais peuvent ajustés lors de la simulation avec des sliders sur la GUI.

La reprojection des positions est quant à elle assez naïve, puisqu'il s'agit d'une simple projection sur le plan :
$$
d = ptc.r - (ptc.p - a) \cdot plane.n \\
ptc.p \mathrel{+}= d * plane.n
$$
 <a name="ACSS"></a>

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/Ea0iqy5.gifv#embed"></iframe>
(commit 1ad263e3)

## Amélioration de la collision sphère-sphère

Le critère de détection ne change pas par rapport à la version naïve, mais tout le reste change.
On peut distinguer deux cas :

- Les collisions élastiques (vitesse relative importante)
- Les contacts statiques (vitesse relative faible)

<a name="ACE"></a>
#### Collision élastique

Pour les collisions élastiques, nous avons utilisés le fait que toutes les sphères avaient la même masse (pour le moment) et donc nous avons interchangés leur vitesse orthogonale. 
Ainsi, pour deux particules `P1`​ et `P2` on a:
$$
u = (p1.p - p2.p)~ / ~||p1.p - p2.p|| \\
P1.v \mathrel{+}= ((P2.v - P1.v) \cdot u)~u \\
P2.v \mathrel{-}= ((P2.v - P1.v) \cdot u)~u
$$

#### Contacts statiques

Pour les contacts statiques, nous avons simplement multiplié les vitesses des sphères par un coefficient $\mu$, compris entre 0 et 1.

Dans les deux cas, les positions des sphères sont mises à jour via une reprojection sur la direction entre les deux sphères:
$$
P1.p \mathrel{+}= \frac{(P1.r + P2.r - ||P1.p - P2.p||)}{2}u \\
P2.p \mathrel{-}= \frac{(P1.r + P2.r - ||P1.p - P2.p||)}{2}u
$$
Les résultats pour les différentes collisions sont meilleures qu'avant mais il subsiste des irrégularités, notamment le fait que les sphères "se collent" entre elles, ce qui semble être anormal.

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/hdLEs93.gifv#embed"></iframe>
(commit cea24055)

#### u est un vecteur unitaire par rapport aux ~~vitesses~~ positions

Une erreur dans notre implémentation qui a vu `u` être un vecteur unitaire par rapport aux vitesses:
$$
u = (p1.v - p2.v)~ / ~||p1.v - p2.v|| \\
$$
Cette erreur corrigée, les résultats avec l'amélioration des collisions sont bien meilleurs, et déjà très réalistes, du moins pour nos yeux:

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/R68fMhv.gifv#embed"></iframe>
(commit 8fb20946)

<a name="ISM"></a>

## Implémentation d'un système de masse

Nous avons ensuite implémenter un système de masse pour nos particules, plus particulièrement pour la collision entre sphères. Ainsi, nous avons ajouté un attribut `m` à la structure représentant les sphères. De plus, le calcul des vitesses lors d'une collision élastique a été revu : 
$$
u = (P1.p - P2.p)~ / ~||P1.p - P2.p|| \\
massCoef = \frac{2}{P1.m + P2.m} \\
P1.v \mathrel{+}= P2.m * massCoef * ((P2.v - P1.v) \cdot u)~u \\
P2.v \mathrel{-}= P1.m * massCoef * ((P2.v - P1.v) \cdot u)~u
$$
Afin de tester ce système, nous avons choisi de "grossir" la 1ère sphère créé, qui a une masse 50 fois supérieure aux autres et un rayon deux fois plus grand. Les résultats sont très probants, et peuvent être vu ici:

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/bHJZn1D.gifv#embed"></iframe>
(commit 5de5106c)

<a name="RC"></a>

## Modification de la gravité en fonction de l'orientation du cube

Cette fonctionnalité fut plus simple à implémenter que prévu, à partir du moment où nous nous sommes rendus compte que la rotation du cube était la même que celle de la caméra, qui est déjà calculée dans le champ `scene.camera.orientation`. Ainsi, la seule modification à faire est de modifier la force appliquée à la sphère de : 
$$
ptc.f = \begin{bmatrix}0, -9.81, 0\end{bmatrix}
$$
à :
$$
ptc.f = scene.camera.orientation * \begin{bmatrix}0, -9.81, 0\end{bmatrix}
$$
Les résultats étaient ceux attendus, on peut maintenant s'amuser à faire tourner notre cube dans tous les sens et les sphères suivront :

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/s2LPt7m.gifv#embed"></iframe>
(commit b446e043)

<a name="TC"></a>

## Prise en compte de la translation du cube

Similairement à la fonctionnalité précédente,  une fois qu'on comprend le fait que la translation du cube est l'inverse du déplacement de la caméra, alors l'ajout d'une force liée à ce déplacement est relativement aisé, d'autant plus que le déplacement est donné dans `scene.camera.translation`, en gardant la valeur de ce champ à l'instant précédent `t-1`.

Ainsi, on a :
$$
ptc.f = scene.camera.orientation * \begin{bmatrix}0, -9.81, 0\end{bmatrix} - 10000 * (scene.camera.translation(t) -scene.camera.translation(t-1))
$$
Le facteur `10000` a été trouvé empiriquement pour avoir une force suffisamment forte pour observer des résultats satisfaisants :

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/Qbwg6Si.gifv#embed"></iframe>
(commit c234ec47)

## Implémentation d'un "attracteur"

Nous avons décidé d'ajouter une fonctionnalité bonus : un point d'attraction activable au centre du cube. Lorsqu'il est activé (via la GUI), le centre du cube attire toutes les sphères avec un coefficient `attractor_force` réglable sur la GUI.

Ainsi on a :
$$
ptc.f = scene.camera.orientation * \begin{bmatrix}0, -9.81, 0\end{bmatrix} - 10000 * (scene.camera.translation(t) -scene.camera.translation(t-1)) \\
if~(attractor)~ then~ ptc.f~ \mathrel{+}= \frac{\begin{bmatrix}0, 0, 0\end{bmatrix} - ptc.p}{||\begin{bmatrix}0, 0, 0\end{bmatrix} - ptc.p||} * attractor\_force
$$

Les résultats varient en fonction de la force qu'on donne au point d'attraction et de la position qu'occupaient les sphères au moment de l'activation de l'"attracteur" :

<iframe class="imgur-embed" width="100%" height="400" frameborder="0" src="https://i.imgur.com/wCGl8T4.gifv#embed"></iframe>
(commit 4d9b54ed)