
#include "sphere_collision.hpp"

#include <random>

#ifdef SCENE_SPHERE_COLLISION

using namespace vcl;

void scene_model::frame_draw(std::map<std::string, GLuint>&, scene_structure& scene, gui_structure&) {
    float dt = 0.02f * timer.scale;
    timer.update();

    set_gui();

    create_new_particle();
    compute_time_step(dt);

    display_particles(scene);
    draw(borders, scene.camera);

    camera_rot = scene.camera.orientation;
    camera_prev = camera_cur;
    camera_cur = scene.camera.translation;
}

void scene_model::compute_time_step(float dt) {
    // Set forces
    const size_t N = particles.size();
    for (size_t k = 0; k < N; ++k) {
        particles[k].f = camera_rot * vec3(0, -9.81f, 0) - 200.f * (camera_cur - camera_prev);
        if (attractor) {
            particles[k].f += normalize(vec3{0, 0, 0} - particles[k].p) * attactor_force;
        }
    }


    // Integrate position and speed of particles through time
    for (size_t k = 0; k < N; ++k) {
        particle_structure& particle = particles[k];
        vec3& v = particle.v;
        vec3& p = particle.p;
        vec3 const& f = particle.f;

        v = (1 - 0.9f * dt) * v + dt * f; // gravity + friction force
        p = p + dt * v;
    }

    // Collisions between spheres
    for (size_t i = 0; i < N; ++i) {
        for (size_t j = i + 1; j < N; ++j) {
            auto& pi = particles[i];
            auto& pj = particles[j];

            const auto diff = pi.p - pj.p;
            const auto u = normalize(diff);

            if (float dist = norm(diff); dist < pi.r + pj.r) {
                const auto delta_v = pj.v - pi.v;
                if (norm(delta_v) < static_threshold) {
                    pi.v *= sphere_friction;
                    pj.v *= sphere_friction;
                } else {
                    const auto mass_coef = 2 / (pi.m + pj.m);
                    const auto v_ortho = dot(delta_v, u) * u;

                    pi.v += pj.m * mass_coef * v_ortho;
                    pj.v -= pi.m * mass_coef * v_ortho;
                }
                pi.p += (pi.r + pj.r - dist) * u * 0.5f;
                pj.p -= (pi.r + pj.r - dist) * u * 0.5f;
            }
        }
    }

    // Collisions with cube
    for (auto& ptc : particles) {
        for (auto& plane : planes) {
            if (float detection = dot(ptc.p - plane.a, plane.n); detection <= ptc.r) {
                auto v_ortho = dot(ptc.v, plane.n) * plane.n;
                auto v_par = ptc.v - v_ortho;
                ptc.v = plane_friction * v_par - plane_impact * v_ortho;
                ptc.p += (ptc.r - detection) * plane.n;
            }
        }
    }
}


void scene_model::create_new_particle() {
    // Emission of new particle if needed
    timer.periodic_event_time_step = gui_scene.time_interval_new_sphere;
    const bool is_new_particle = timer.event;
    static const std::vector<vec3> color_lut = {{1, 0, 0},
                                                {0, 1, 0},
                                                {0, 0, 1},
                                                {1, 1, 0},
                                                {1, 0, 1},
                                                {0, 1, 1}};

    if (is_new_particle && gui_scene.add_sphere) {
        particle_structure new_particle;

        new_particle.r = 0.08f;
        new_particle.c = color_lut[int(rand_interval() * color_lut.size())];

        // Initial position
        new_particle.p = vec3(0, 0, 0);

        // Initial speed
        const float theta = rand_interval(0, 2 * 3.14f);
        new_particle.v = vec3(2 * std::cos(theta), 5.0f, 2 * std::sin(theta));

        new_particle.m = 1.f;

        if (particles.empty()) {
            new_particle.r *= 2;
            new_particle.m *= 50;
        }

        particles.push_back(new_particle);

    }
}

void scene_model::display_particles(scene_structure& scene) {
    const size_t N = particles.size();
    for (size_t k = 0; k < N; ++k) {
        const particle_structure& part = particles[k];

        sphere.uniform.transform.translation = part.p;
        sphere.uniform.transform.scaling = part.r;
        sphere.uniform.color = part.c;
        draw(sphere, scene.camera);
    }
}


void scene_model::setup_data(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure&) {
    sphere = mesh_drawable(mesh_primitive_sphere(1.0f));
    sphere.shader = shaders["mesh"];

    std::vector<vec3> borders_segments = {{-1, -1, -1},
                                          {1,  -1, -1},
                                          {1,  -1, -1},
                                          {1,  1,  -1},
                                          {1,  1,  -1},
                                          {-1, 1,  -1},
                                          {-1, 1,  -1},
                                          {-1, -1, -1},
                                          {-1, -1, 1},
                                          {1,  -1, 1},
                                          {1,  -1, 1},
                                          {1,  1,  1},
                                          {1,  1,  1},
                                          {-1, 1,  1},
                                          {-1, 1,  1},
                                          {-1, -1, 1},
                                          {-1, -1, -1},
                                          {-1, -1, 1},
                                          {1,  -1, -1},
                                          {1,  -1, 1},
                                          {1,  1,  -1},
                                          {1,  1,  1},
                                          {-1, 1,  -1},
                                          {-1, 1,  1}};
    borders = segments_gpu(borders_segments);
    borders.uniform.color = {0, 0, 0};
    borders.shader = shaders["curve"];

    planes = {
            {{0,  -1, 0},  {0,  1,  0}},
            {{0,  1,  0},  {0,  -1, 0}},
            {{-1, 0,  0},  {1,  0,  0}},
            {{1,  0,  0},  {-1, 0,  0}},
            {{0,  0,  -1}, {0,  0,  1}},
            {{0,  0,  1},  {0,  0,  -1}}
    };

    plane_friction = 0.9f;
    plane_impact = 0.7f;
    static_threshold = 0.05f;
    sphere_friction = 0.8f;
    camera_rot = scene.camera.orientation;
    camera_prev = scene.camera.camera_position();
    camera_cur = camera_prev;
}


void scene_model::set_gui() {
    // Can set the speed of the animation
    ImGui::SliderFloat("Time scale", &timer.scale, 0.05f, 2.0f, "%.2f s");
    ImGui::SliderFloat("Interval create sphere", &gui_scene.time_interval_new_sphere, 0.05f, 2.0f, "%.2f s");
    ImGui::SliderFloat("Plane Friction", &plane_friction, 0.05f, 1.0f, "%.2f");
    ImGui::SliderFloat("Plane Impact", &plane_impact, 0.05f, 1.0f, "%.2f");
    ImGui::SliderFloat("Static Threshold", &static_threshold, 0.01f, 0.1f, "%.3f m/s");
    ImGui::SliderFloat("Sphere Friction", &sphere_friction, 0.05f, 1.0f, "%.2f");
    ImGui::Checkbox("Add sphere", &gui_scene.add_sphere);
    ImGui::Checkbox("Attractor", &attractor);
    ImGui::SliderFloat("Attractor Force", &attactor_force, 1.f, 100.f, "%.2f m/s2");

    bool stop_anim = ImGui::Button("Stop");
    ImGui::SameLine();
    bool start_anim = ImGui::Button("Start");

    if (stop_anim) timer.stop();
    if (start_anim) timer.start();
}


#endif
